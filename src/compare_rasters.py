import arcpy
import os
import sys
import math
import urllib2
import json


root = "D:\\airphoto_processing"
updates = ["Aug122013", "Oct102013"]

for update in updates:
	print "\n" 
	print "Comparing SRs for " + update 
	years = os.listdir(os.path.join(root, update))
	for year in years:		
		sr4269_path = os.path.join(root, update, year, "sr4269")
		sr102100_path = os.path.join(root, update, year, "sr102100")
		arcpy.env.workspace = sr4269_path
		sr4269_list = arcpy.ListRasters()
		sr4269_count = len(sr4269_list)
		arcpy.env.workspace = sr102100_path
		sr102100_list = arcpy.ListRasters()
		sr102100_count = len(sr102100_list)			
		print year + ":\t" + "sr4269 = " + str(sr4269_count) + "\tsr102100 = " + str(sr102100_count) + "\tdiscrepency: " + str(int(math.fabs(sr4269_count - sr102100_count)))
		
		
# now, we need a dictionary of all possible years
all_years = {}
for update in updates:
	for year in os.listdir(os.path.join(root, update)):
		if year not in all_years:
			all_years[year] = []
		
# we're only checking sr102100

for year in all_years:
	for update in updates:
		if os.path.exists(os.path.join(root, update, year, "sr102100")):
			arcpy.env.workspace = os.path.join(root, update, year, "sr102100")
			rasters = arcpy.ListRasters() 
			for raster in rasters:
				if raster not in all_years[year]:
					all_years[year].append(raster)

print "\n"
print "\nComparing resources online...\n"
for year in all_years:
	print "\nYear: " + year
	# these are BLOCKING calls
	get = urllib2.urlopen("http://albers.trentu.ca/arcgis/rest/services/airphoto/"+ year+ "/MapServer/0/query?f=json&where=1%3D1&returnCountOnly=true")
	resp = get.read()
	#print str(len(str(resp)))
	print "Archived: " + str(len(all_years[year])) + ", Online: " + resp[9:len(str(resp))-1]

	
#resp = urllib2.urlopen("http://albers.trentu.ca/arcgis/rest/services/airphoto/y1960/MapServer/0/query?f=json&where=1%3D1&returnCountOnly=true")
#j = resp.read()
#print j


	
		
	
	
	