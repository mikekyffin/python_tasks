import arcpy
import sys, os
import arcinfo
import getopt

# this is a command line utility
def main(argv):
	# var to hold the in and out directories
	dir = ""
	# get args from command line
	try:
		opts, args = getopt.getopt(argv,"hi:",["idir="])
	except getopt.GetoptError:
		print "check_sr.py -i <inputfile>"
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print "File: check_sr.py\n"
			print ""
			print "check_sr.py -i <input directory>"
			sys.exit()
		elif opt in ("-i", "--idir"):
			dir = arg
	print "...preparing to validate all files in " + dir	
	read(dir)
	print "...done!"

def read(dir):
	arcpy.env.workspace = dir
	rasters = arcpy.ListRasters()
	for raster in rasters:
		name = arcpy.Describe(raster).name
		sr = arcpy.Describe(raster).spatialReference.name
		if sr != "GCS_North_American_1983":
			print name + ": " + sr	
	
# TODO create validation message here: def report(numCorrect, numIncorrect)	
if __name__ == "__main__":
   main(sys.argv[1:])
