import arcpy
import arcinfo
import sys
import os
import getopt
import time
import glob

# this is a command line utility
def main(argv):
    # vars to hold the in and out directories
    inputDir = ""
    outputDir = ""
    # get args from command line
    try:
        opts, args = getopt.getopt(argv,"hi:o:",["idir=","odir="])
    except getopt.GetoptError:
        print 'batch_project_airphotos.py -i <input directory> -o <output directory>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
			print "File: batch_project_airphotos.py\n"
			print "Projects an entire folder of NAD83 air photos to Web Mercator for air photo portion of REHA project"
			print "batch_project_airphotos.py -i <input directory> -o <output directory>"
			sys.exit()
        elif opt in ("-i", "--idir"):
            inputDir = arg
        elif opt in ("-o", "--odir"):
            outputDir = arg 
    print "...preparing to project all files in " + inputDir + " to output folder " + outputDir     
    projectAll(inputDir, outputDir)
    clean(outputDir)
    print "...done!"

def clean(dir):
    print "...cleaning unnecessary files. Please wait."
    files= glob.glob(dir+"/*.ovr") + glob.glob(dir+"/*.dbf")
    for filename in files:
        print "...removing: " + filename
        os.unlink(filename)

def projectAll(inDir, outDir):
    try:
        arcpy.env.overwriteOutput = True
        # set the input folder as the arcpy env so we can easily read raster
        # dataset from it.
        arcpy.env.workspace = inDir
        #
        inSr = arcpy.SpatialReference(4269)
        outSr = arcpy.SpatialReference(102100)
        # gimme some rasters!
        rasters = arcpy.ListRasters()
        num = len(rasters)
        timeMsg = "<waiting for data>"
        print "...found " + str(num) + " rasters to project"    
        tm = TimeMessage(num)
        for raster in rasters:          
            startTime = time.time()
            # do some actual work. thanks.
            arcpy.ProjectRaster_management(os.path.join(inDir, raster), os.path.join(outDir, raster), outSr, "#", "#", "#", "#", inSr)                      
            elapsedTime = time.time() - startTime
            tm.add(elapsedTime)
            # just a friendly message
            print "Converted: " + raster + ". " + str(num) + " raster(s) remaining - approximately " + tm.getAvgAsStringMessage()
            num = num - 1
    except NameError, e:
        print "Oops! Check this: ", e.message
    except Exception, e:
        print "Oops! Check this: ", e.message
                
class TimeMessage:
    totalDatasets = 0
    totalProcessed = 0
    totalTimeElapsed = 0
    totalProjectedTime = 0
    remainderProjected = 0
    averageTime = 0 
    def __init__(self, _numOfDatasets):
        self.totalDatasets = _numOfDatasets
        self.data = []
    def add(self, _time):
		self.totalTimeElapsed = self.totalTimeElapsed + _time
		self.totalProcessed = self.totalProcessed + 1
		self.averageTime = self.totalTimeElapsed / self.totalProcessed
		self.projectedTotal = self.totalDatasets * self.averageTime
		self.remainderProjected = self.projectedTotal - (self.totalProcessed * self.averageTime)
    def getAvgAsStringMessage(self):        
        return str(round((self.remainderProjected / 60), 2)) + " minutes"    
                                                
if __name__ == "__main__":
   main(sys.argv[1:])