############
# what: 	get_map.py
# who: 		Mike Kyffin, GIS Developer
# where: 	The Maps, Data and Government Information Centre (MaDGIC) @ Trent University (Peterborough, Canada)
# why: 		Facilitate downloads of single (archived) 
############

import arcpy
import os
import zipfile
import shutil

# return the extension of the file
def getExtension(file_name):
	return file_name[file_name.rfind("."):]

# zip and close the supplied zip file at the folder path given		
def zipOutputFile(path, zip_file):	
	for fileName in filter(path):
		arcpy.AddMessage("Adding: '" + fileName + "' to zip archive.")
		zip_file.write(os.path.join(path, fileName), arcname=fileName)	
	zip_file.close()
	
# filter 	
def filter(path):
	for file in os.listdir(path):
		if os.path.isfile(os.path.join(path, file)):
			if file != "messages.xml" and getExtension(file) not in [".lock", ".gdb", ".zip", ".ovr", ".rrd", ".dbf", ".sd", ".mxd"]:
				yield file	
	
# MAIN		
# helper functions above
if __name__ == '__main__':
	try:
		# constants 
		DATA_ROOT = "\\\\sanmiguel\\Data\\arcgisdata\\" 	# what is the root of the data involved?	
		WORK_SPACE = arcpy.env.scratchWorkspace	# remember the server-assigned arcgisjobs workspace - we'll need this later.
		
		# get the parameters needed
		dataset_name = arcpy.GetParameterAsText(0)
		project_name = arcpy.GetParameterAsText(1)
		output_file = arcpy.SetParameterAsText(2, os.path.join(WORK_SPACE, dataset_name + ".zip"))

		# the resultant file
		output_path = os.path.join(WORK_SPACE, dataset_name + ".zip") 
		zip = zipfile.ZipFile(output_path, "w")

		# deterimine the project folder
		arcpy.env.workspace = os.path.join(DATA_ROOT, project_name)
		# this is the default - we have A LOT of .tif files
		extension = ".tif" 
		rasters = arcpy.ListRasters()
		for raster in rasters:
			if raster.startswith(dataset_name):
				extension = raster[len(raster)-4:]
			
		for root, dirs, files in os.walk(arcpy.env.workspace):
			for file in files:
				if file.startswith(dataset_name):
					shutil.copy(os.path.join(root, file), WORK_SPACE)
					
		# zip up the files
		zipOutputFile(WORK_SPACE, zip)
		# clean your room!
		arcpy.Delete_management(os.path.join(WORK_SPACE, dataset_name + extension))
	except Exception as e:
		arcpy.AddMessage(str(e))
