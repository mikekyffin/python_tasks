# Esri start of added variables
import os, arcpy
g_ESRI_variable_1 = u'\\\\sanmiguel\\Data\\arcgisdata\\'
# Esri end of added variables

# Esri start of added imports
import sys
import zipfile

# return the extension of the file
def getExtention(file_name):
		return file_name[file_name.index('.'):]

# zip and close the supplied zip file at the folder path given		
def zipOutputFile(path, zip_file):
	for (dirpath, dirnames, filenames) in os.walk(path):
		for dirname in dirnames:
			arcpy.AddMessage("directory name: " + dirname)
			#if dirname == "scratch":
			for file in filenames:
				# Ignore .lock files
				#if not file.endswith('.lock') | file.endswith('.gdb') | file.endswith('.zip'):
				if file != "messages.xml" and getExtention(file) not in [".lock", ".gdb", ".zip"]:
					arcpy.AddMessage("adding: " + file + " to zip archive.")
					zip.write(os.path.join(dirpath, file), arcname=file)				
	zip.close()	



	
if __name__ == '__main__':
	# constants 
	DATA_ROOT = g_ESRI_variable_1 	# what is the root of the data involved?
	WORK_SPACE = arcpy.env.scratchWorkspace				# remember the server-assigned arcisjobs workspace - we'll need this later.

	# get the parameters needed
	xmin = arcpy.GetParameterAsText(0)
	xmax = arcpy.GetParameterAsText(1)
	ymin = arcpy.GetParameterAsText(2)
	ymax = arcpy.GetParameterAsText(3)
	dataset_name = arcpy.GetParameterAsText(4)
	project_name = arcpy.GetParameterAsText(5)
	output_file = arcpy.SetParameterAsText(6, os.path.join(WORK_SPACE, dataset_name + ".zip"))

	# the resultant file
	output_path = os.path.join(WORK_SPACE, dataset_name + ".zip") 
	zip = zipfile.ZipFile(output_path, 'w')

	# deterimine the project gdb path
	db_path = os.path.join(DATA_ROOT, project_name)

	# determine the feature class to be clipped
	input_file = os.path.join(db_path + "\\" + project_name + ".gdb", dataset_name)
		
	# create the envelope
	array = arcpy.Array()
	array.add(arcpy.Point(xmin, ymax))
	array.add(arcpy.Point(xmax, ymax))
	array.add(arcpy.Point(xmax, ymin))
	array.add(arcpy.Point(xmin, ymin))
	array.add(arcpy.Point(xmin, ymax))

	# (optional) add a spatial reference to the clip polygon 
	sr = arcpy.SpatialReference()
	sr.factoryCode = 102100
	sr.create()

	clip_polygon = arcpy.Polygon(array, sr)
	arcpy.Clip_analysis(input_file, clip_polygon, output_file)
	zipOutputFile(WORK_SPACE, zip)


		

